package org.nrg.dcm.xnat;

import org.dcm4che2.data.Tag;
import org.nrg.dcm.AttrDefs;
import org.nrg.dcm.MutableAttrDefs;

public class XAScanAttributes {
    private XAScanAttributes() {} // no instantiation

    static public AttrDefs get() { return s; }

    static final private MutableAttrDefs s = new MutableAttrDefs(ImageScanAttributes.get());

    static {
        s.add(new OrientationAttribute("parameters/orientation"));
        s.add("parameters/imageType", Tag.ImageType);
        s.add(new ImageFOVAttribute("parameters/fov"));
        s.add("parameters/derivation", Tag.DerivationDescription);
        s.add("parameters/options", Tag.ScanOptions);
        s.add("parameters/derivation", Tag.DerivationDescription);
    }
}
